lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

tokens
{
  TK_class
}

PROGRAM: 'Program';
IF: 'if';
BOOLEAN: 'boolean';
CALLOUT: 'callout';
CLASS: 'class';
ELSE: 'else';
INTEGER: 'int';
RETURN: 'return';
VOID: 'void';
FOR: 'for';
BREAK: 'break';
CONTINUE: 'continue';

OPERACAO_SOMA: ( '+' | '-' | '*' | '/' | '%' );
OPERACAO_COMPARADOR: ( '<' | '>' | '<=' | '>=' );
OPERACAO_IGUALDADE: ( '==' | '!=' );
OPERACAO_AND: ( '&&' | '||' );
OPERACAO_ATRIBUICAO: ( '+=' | '-=');
OPERACAO_RESULTADO: ( '=');

NEGATIVE: '-';
BOOLEANLITERAL: ('true' | 'false');

OPENPARENTHESIS: '(';
OPENBRACKET: '[';
OPENCURLY : '{';

CLOSEPARENTHESIS: ')';
CLOSEBRACKET: ']';
CLOSECURLY : '}';

PONTEXT: (','| ';' | '!');

ID: ('a'..'z' | 'A'..'Z' | '_' | '%' )+  ([0-9])* ID?;

INTLITERAL: [0-9]+ ('x' ([a-fA-F] | [0-9])+)?;
DECLVAR: [1-9]+ ;

WS_ : (' ' | '\n'| '\t' ) -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' ( [ !#-&(-.0-Z^-~] | INTEIRO | ESC) '\'';

STRINGLITERAL : '"' (ID | PONTUACAO)+ '"';

fragment INTEIRO: [0-9];
fragment PONTUACAO: ( '.' | '?' | ',' | ';' | ' ' | ':'| '!' | ESPECIAL);
fragment ESPECIAL: '\\' ( '\'' | '\"' | '\\' | ID );
fragment ESC :  '\\' ('n'|'t'|'\\'|'"');
