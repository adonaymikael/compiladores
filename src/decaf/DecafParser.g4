parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: 'class' 'Program' OPENCURLY (var_decl+)* method_decl* CLOSECURLY ;

method_decl: ( type | VOID ) ID OPENPARENTHESIS ((var_decl)+ PONTEXT* )? CLOSEPARENTHESIS block_decl  ;

block_decl: OPENCURLY var_decl* statement_decl* CLOSECURLY ;

var_decl: type (ID PONTEXT* )* array_decl? PONTEXT*;
array_decl: OPENBRACKET INTLITERAL CLOSEBRACKET; 

type: INTEGER | BOOLEAN;
type_decl: type ID;

statement_decl: location_decl assign_op expr_decl PONTEXT
|	method_call PONTEXT
|	IF (expr_decl block_decl) ( ELSE  block_decl )?
|	FOR ID OPERACAO_RESULTADO expr_decl PONTEXT* expr_decl block_decl
|	RETURN expr_decl? PONTEXT
|	BREAK PONTEXT
|	CONTINUE PONTEXT
|	block_decl ;

assign_op: OPERACAO_ATRIBUICAO | OPERACAO_RESULTADO ;

method_call: ID OPENPARENTHESIS expr_decl (PONTEXT expr_decl)* CLOSEPARENTHESIS
|	CALLOUT OPENPARENTHESIS STRINGLITERAL ( PONTEXT* (expr_decl | STRINGLITERAL)+  PONTEXT* )? CLOSEPARENTHESIS ;

location_decl : ID | ID OPENBRACKET expr_decl CLOSEBRACKET ;

expr_decl: location_decl 
|	method_call 
|	identifier_decl
|	expr_decl bin_op expr_decl 
|	bin_op expr_decl* 
|	PONTEXT expr_decl*
|	OPENPARENTHESIS expr_decl CLOSEPARENTHESIS ;

bin_op: OPERACAO_SOMA | OPERACAO_COMPARADOR | OPERACAO_IGUALDADE | OPERACAO_AND ;
identifier_decl: INTLITERAL INTLITERAL* | INTLITERAL | CHAR | BOOLEANLITERAL ;